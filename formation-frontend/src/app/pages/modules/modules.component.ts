import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatDrawer } from '@angular/material/sidenav';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { filter, map, mergeMap, Observable, startWith } from 'rxjs';
import { Formateur } from 'src/app/models/formateur';
import { Groupe, groupeForm } from 'src/app/models/groupe';
import { Module } from 'src/app/models/module';
import { Stagiaire } from 'src/app/models/stagiaires';
import { FormateurService } from 'src/app/services/formateur.service';
import { GroupeService } from 'src/app/services/groupe.service';
import { ModuleService } from 'src/app/services/module.service';
import { StagiaireService } from 'src/app/services/stagiaire.service';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  @ViewChild("table") table?: MatTable<any>;
  @ViewChild("drawer") drawer?: MatDrawer;
  // @ViewChild('groupeInput') groupeInput?: ElementRef<HTMLInputElement>;

  modules: Module[] = [];
  selectedModule?: Module;

  formateurs: Formateur[] = [];

  sideTitle: string = "";
  moduleFormGroupe: FormGroup = Module.formGroup();

  // stagiaireFormControl = new FormControl();
  // formateurFormControl: FormControl = new FormControl();
  // filteredStagiaires: Observable<Stagiaire[]>;

  constructor(
    public ms: ModuleService,
    public fs: FormateurService,
    public ss: StagiaireService,
    public gs: GroupeService,
    public dialog: MatDialog,
    private router: Router,
  ) {
    // this.filteredStagiaires = this.stagiaireFormControl.valueChanges.pipe(
    //   startWith(""),
    //   filter(v => typeof v === "string"),
    //   mergeMap(q => ss.search(q, 10))
    // );
  }

  ngOnInit(): void {
    this.ms.findAll().subscribe(lm => this.modules = lm);
    this.fs.findAll().subscribe(lf => this.formateurs = lf);
  }

  onAdd() {
    this.selectedModule = { id: 0, nom: "", description: "", code: "", formateur: undefined, jours: [], nbParticipations: 0};
    this.sideTitle = "Nouveau module";
    // this.groupesFormArray.clear();
    // this.participantsFormArray.clear();
    this.moduleFormGroupe.reset();
    this.moduleFormGroupe.setValue({ nom: "", description: "", formateur: {id: 0}, jours: [] });
    if (!this.drawer?.opened)
      this.drawer?.open();
  }

  onDelete() {
    if (this.selectedModule) {
      let m = this.selectedModule;
      this.ms.delete(m).subscribe({
        next: () => {
          this.modules.splice(this.modules.indexOf(m), 1);
          this.table?.renderRows();
          this.drawer?.close();
        }
      });
    }
  }

  onSelect(m: Module) {
    if (this.selectedModule == m && this.drawer?.opened) {
      this.drawer?.close();
    } else {
      this.selectedModule = m;
      this.sideTitle = "Module " + m.id;
      this.moduleFormGroupe.reset();
      // this.participantsFormArray.clear();
      this.moduleFormGroupe.setValue({
        nom: m.nom,
        description: m.description,
        formateur: {id: m.formateur ? m.formateur.id : 0},
        // participations: m ? m.participations.map(p => {id: p.id}) : [],
        jours: m.jours ?? [] });
      if (!this.drawer?.opened)
        this.drawer?.open();
    }
  }

  onCancel() {
    this.selectedModule = undefined;
    this.drawer?.close();
  }

  onSubmit() {
    if (this.moduleFormGroupe.invalid) {
      this.moduleFormGroupe.markAllAsTouched();
      return;
    }
    if (this.selectedModule) {
      let m = this.selectedModule;
      // let lg = this.moduleFormGroupe.get("groupes")?.value;
      Object.assign(m, this.moduleFormGroupe.value);
      if (m.formateur?.id == 0)
        m.formateur = undefined;
      // g.nom = this.moduleFormGroupe.get("nom")?.value;
      // g.nbStagiaires = ls.length;
      if (m.id != 0) {
        this.ms.update(m).subscribe({
          next: () => {
            // this.ss.saveGroupes(s, lg).subscribe({
            //   next: () => {
            //     // this.drawer?.close();
                this.moduleFormGroupe.markAsPristine();
                this.table?.renderRows();
            //   }
            // });
          }
        });
      } else {
        this.ms.save(m).subscribe({
          next: m => {
            // this.ss.saveGroupes(s, lg).subscribe({
            //   next: () => {
                this.drawer?.close();
                this.modules.push(m);
                this.table?.renderRows();
            //   }
            // });
          }
        });
      }
    }
  }

  addParticipation(event: MatAutocompleteSelectedEvent) {
    console.log(event.option.value)
    let s = event.option.value as Stagiaire;
    this.participantsFormArray.push(
      new FormGroup({
        id: new FormControl(0),
        stagiaire: Stagiaire.formGroup(s)
      })
    );
  //     // this.modulesFormateurFormControl.setValue({ id: f.id });
  //   // if (this.groupeInput)
  //   //   this.groupeInput.nativeElement.value = "";
  //   // this.groupeControl.setValue("");
  //   // this.groupeControl.updateValueAndValidity();
  //   // this.groupesFormArray.markAsDirty();
  }

  // selectFormateur(event: MatAutocompleteSelectedEvent) {
  //   let f = event.option.value as Formateur;
  //   this.modulesFormateurFormControl.setValue({ id: f.id });
  //   // if (this.groupeInput)
  //   //   this.groupeInput.nativeElement.value = "";
  //   // this.groupeControl.setValue("");
  //   // this.groupeControl.updateValueAndValidity();
  //   // this.groupesFormArray.markAsDirty();
  // }

  // removeGroupe(index: number) {
  //   this.groupesFormArray.removeAt(index);
  //   this.groupeControl.updateValueAndValidity();
  //   this.groupesFormArray.markAsDirty();
  // }

  // get groupesFormArray(): FormArray {
  //   return this.moduleFormGroupe.get("groupes") as FormArray;
  // }

  // get modulesFormateurFormControl(): FormGroup {
  //   return this.moduleFormGroupe.get("formateur") as FormGroup;
  // }

  // formateurToStr(f: Formateur) : string {
  //   return f ? f.prenom + " " + f.nom : "";
  // }

  removeParticipation(index: number) {
    this.participantsFormArray.removeAt(index);
    this.participantsFormArray.markAsDirty();
    this.moduleFormGroupe.updateValueAndValidity();
  }

  get participantsFormArray(): FormArray {
    return this.moduleFormGroupe.get("participations") as FormArray;
  }

}

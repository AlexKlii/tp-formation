import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatDrawer } from '@angular/material/sidenav';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { filter, map, mergeMap, Observable, startWith } from 'rxjs';
import { Formateur } from 'src/app/models/formateur';
import { Groupe, groupeForm } from 'src/app/models/groupe';
import { Stagiaire } from 'src/app/models/stagiaires';
import { FormateurService } from 'src/app/services/formateur.service';
import { GroupeService } from 'src/app/services/groupe.service';
import { StagiaireService } from 'src/app/services/stagiaire.service';

@Component({
  selector: 'app-formateurs',
  templateUrl: './formateurs.component.html',
  styleUrls: ['./formateurs.component.css']
})
export class FormateursComponent implements OnInit {

  @ViewChild("table") table?: MatTable<any>;
  @ViewChild("drawer") drawer?: MatDrawer;

  formateurs: Formateur[] = [];
  selectedFormateur?: Formateur;

  sideTitle: string = "";
  formateurFormGroupe: FormGroup = Formateur.formGroup();

  constructor(
    public fs: FormateurService,
    public ss: StagiaireService,
    public gs: GroupeService,
    public dialog: MatDialog,
    private router: Router,
  ) {
    // this.filteredGroupes = this.groupeControl.valueChanges.pipe(
    //   startWith(""),
    //   filter(v => typeof v === "string"),
    //   mergeMap(q => gs.search(q, 10)),
    //   map(lg => lg.filter(g => !(this.groupesFormArray.value as Groupe[]).find(g1 => g.id == g1.id)))
    // );
  }

  ngOnInit(): void {
    this.fs.findAll().subscribe(lf => this.formateurs = lf);
  }

  onAdd() {
    this.selectedFormateur = { id: 0, prenom: "", nom: "", email: "", motDePasse: "", role: "FORMATEUR", modulesNb: 0};
    this.sideTitle = "Nouveau formateur";
    // this.groupesFormArray.clear();
    this.formateurFormGroupe.reset();
    this.formateurFormGroupe.setValue({ prenom: "", nom: "", email: "" });
    if (!this.drawer?.opened)
      this.drawer?.open();
  }

  onDelete() {
    if (this.selectedFormateur) {
      let f = this.selectedFormateur;
      this.fs.delete(f).subscribe({
        next: () => {
          this.formateurs.splice(this.formateurs.indexOf(f), 1);
          this.table?.renderRows();
          this.drawer?.close();
        }
      });
    }
  }

  onSelect(f: Formateur) {
    if (this.selectedFormateur == f && this.drawer?.opened) {
      this.drawer?.close();
    } else {
      this.selectedFormateur = f;
      this.sideTitle = "Formateur " + f.id;
      // this.ss.getGroupes(s).subscribe(lg => {
        this.formateurFormGroupe.reset();
      //   this.groupesFormArray.clear();
        this.formateurFormGroupe.setValue({ prenom: f.prenom, nom: f.nom, email: f.email });
      //   lg.forEach(g => this.groupesFormArray.push(new FormControl(g)));
      // });
      if (!this.drawer?.opened)
        this.drawer?.open();
    }
  }

  onCancel() {
    this.selectedFormateur = undefined;
    this.drawer?.close();
  }

  onSubmit() {
    if (this.formateurFormGroupe.invalid) {
      this.formateurFormGroupe.markAllAsTouched();
      return;
    }
    if (this.selectedFormateur) {
      let f = this.selectedFormateur;
      // let lg = this.formateurFormGroupe.get("groupes")?.value;
      Object.assign(f, this.formateurFormGroupe.value);
      // g.nom = this.formateurFormGroupe.get("nom")?.value;
      // g.nbStagiaires = ls.length;
      if (f.id != 0) {
        this.fs.update(f).subscribe({
          next: () => {
            // this.ss.saveGroupes(s, lg).subscribe({
            //   next: () => {
            //     // this.drawer?.close();
                this.formateurFormGroupe.markAsPristine();
            //   }
            // });
          }
        });
      } else {
        this.fs.save(f).subscribe({
          next: f => {
            // this.ss.saveGroupes(s, lg).subscribe({
            //   next: () => {
                this.drawer?.close();
                this.formateurs.push(f);
                this.table?.renderRows();
            //   }
            // });
          }
        });
      }
    }
  }

  // addGroupe(event: MatAutocompleteSelectedEvent) {
  //   this.groupesFormArray.push(new FormControl(event.option.value as Groupe));
  //   if (this.groupeInput)
  //     this.groupeInput.nativeElement.value = "";
  //   this.groupeControl.setValue("");
  //   this.groupeControl.updateValueAndValidity();
  //   this.groupesFormArray.markAsDirty();
  // }

  // removeGroupe(index: number) {
  //   this.groupesFormArray.removeAt(index);
  //   this.groupeControl.updateValueAndValidity();
  //   this.groupesFormArray.markAsDirty();
  // }

  // get groupesFormArray(): FormArray {
  //   return this.formateurFormGroupe.get("groupes") as FormArray;
  // }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Formateur } from '../models/formateur';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class FormateurService extends GenericService<Formateur> {

  constructor(hc: HttpClient) {
    super(hc, environment.backendUrl + "/formateurs");
   }

   search(q: string, nb: number = 0): Observable<Formateur[]> {
    return this.hc.get<Formateur[]>(this.url + "/search?q=" + q + (nb ? ("&page=0&size="+nb) : ""));
  }

  //  search(q: string, nb: number = 0): Observable<Stagiaire[]> {
  //   return this.hc.get<Stagiaire[]>(this.url + "/search?q=" + q + (nb ? ("&page=0&size="+nb) : ""));
  // }

  // getGroupes(s: Stagiaire): Observable<Groupe[]> {
  //   return this.hc.get<Groupe[]>(this.url + "/" + s.id + "/groupes");
  // }

  // saveGroupes(s: Stagiaire, lg: Groupe[]): Observable<any> {
  //   return this.hc.post(this.url + "/" + s.id + "/groupes", lg);
  // }

}

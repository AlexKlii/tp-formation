import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Groupe } from '../models/groupe';
import { Participation } from '../models/participation';
import { Stagiaire } from '../models/stagiaires';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class StagiaireService extends GenericService<Stagiaire> {

  constructor(hc: HttpClient) {
    super(hc, environment.backendUrl + "/stagiaires");
   }

   search(q: string, nb: number = 0): Observable<Stagiaire[]> {
    return this.hc.get<Stagiaire[]>(this.url + "/search?q=" + q + (nb ? ("&page=0&size="+nb) : ""));
  }

  getGroupes(s: Stagiaire): Observable<Groupe[]> {
    return this.hc.get<Groupe[]>(this.url + "/" + s.id + "/groupes");
  }

  saveGroupes(s: Stagiaire, lg: Groupe[]): Observable<any> {
    return this.hc.post(this.url + "/" + s.id + "/groupes", lg);
  }

  getParticipations(s: Stagiaire): Observable<Participation[]> {
    return this.hc.get<Participation[]>(this.url + "/" + s.id + "/participations");
  }

  saveParticipations(s: Stagiaire, lp: Participation[]): Observable<any> {
    return this.hc.post<any>(this.url + "/" + s.id + "/participations", lp);
  }

}

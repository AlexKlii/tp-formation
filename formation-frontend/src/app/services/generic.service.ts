import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface HasId {
  id: number;
}


export class GenericService<T extends HasId> {

  constructor(protected hc: HttpClient, protected url: string) { }


  findAll(): Observable<T[]> {
    return this.hc.get<T[]>(this.url);
  }

  findById(id: number): Observable<T> {
    return this.hc.get<T>(this.url + "/" + id);
  }

  save(a: T): Observable<any> {
    return this.hc.post(this.url, a);
  }

  update(a: T): Observable<any> {
    return this.hc.put(this.url + "/" + a.id, a);
  }

  delete(a: T): Observable<any> {
    return this.deleteById(a.id);
  }

  deleteById(id: number): Observable<any> {
    return this.hc.delete(this.url + "/" + id);
  }
}

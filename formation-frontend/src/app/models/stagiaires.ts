import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { Personne } from "./personne";

export interface Stagiaire extends Personne {
  groupeNames: string[];
  participationsNb: number;
}


export namespace Stagiaire {

  export function formGroup(s?: Stagiaire): FormGroup {
    return new FormGroup({
      prenom: new FormControl( s ? s.prenom : "", [Validators.required, Validators.minLength(3)]),
      nom: new FormControl( s ? s.nom : "", [Validators.required, Validators.minLength(3)]),
      email: new FormControl( s ? s.email : "", [ Validators.email ]),
      groupes : new FormArray([]),
      participations : new FormArray([]),
    })
  }
}

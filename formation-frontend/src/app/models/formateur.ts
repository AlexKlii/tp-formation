import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Personne } from "./personne";

export interface Formateur extends Personne {
  modulesNb: number;
}



export namespace Formateur {

  export function formGroup(f?: Formateur): FormGroup {
    return new FormGroup({
      prenom: new FormControl( f ? f.prenom : "", [Validators.required, Validators.minLength(3)]),
      nom: new FormControl( f ? f.nom : "", [Validators.required, Validators.minLength(3)]),
      email: new FormControl( f ? f.email : "", [ Validators.email ]),
    })
  }
}

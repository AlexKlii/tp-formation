import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { Formateur } from "./formateur";
import { Participation } from "./participation";
import { Stagiaire } from "./stagiaires";

export interface Module {
  id: number;
  nom: string;
  description: string;
  code: string;
  jours: Date[];
  formateur?: Formateur;
  nbParticipations: number;

}

export namespace Module {

  export function formGroup(m?: Module): FormGroup {
    return new FormGroup({
      nom: new FormControl( m ? m.nom : "", [Validators.required, Validators.minLength(3)]),
      description: new FormControl( m ? m.nom : "" ),
      formateur: new FormGroup({
        id: new FormControl( m ? m.formateur?.id : 0 ),
      }),
      jours: new FormArray( m ? m.jours.map(d => new FormControl(d)) : [] )
    })
  }
}

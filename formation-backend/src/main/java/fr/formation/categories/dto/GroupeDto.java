package fr.formation.categories.dto;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import fr.formation.categories.models.HasId;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class GroupeDto implements HasId<Integer> {
	
	private Integer id;
	
	@NotEmpty
	@Length(min = 3)
	private String nom;
	
	private int nbStagiaires;

}

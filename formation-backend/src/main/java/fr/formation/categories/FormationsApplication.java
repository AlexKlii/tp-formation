package fr.formation.categories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import fr.formation.categories.repositories.PersonneRepository;

@SpringBootApplication
public class FormationsApplication {
	
	@Value("${frontend.url}")
	private String frontendUrl;

	
	public static void main(String[] args) {
		SpringApplication.run(FormationsApplication.class, args);
	}


	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "UPDATE", "DELETE", "OPTIONS")
						.allowedOrigins(frontendUrl);
			}

			public void addResourceHandlers(ResourceHandlerRegistry registry) {
				registry.addResourceHandler("/assets/**").addResourceLocations("classpath:/assets/");
			}
		};
	}

}

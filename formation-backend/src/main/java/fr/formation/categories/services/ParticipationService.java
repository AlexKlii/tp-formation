package fr.formation.categories.services;

import org.springframework.stereotype.Service;

import fr.formation.categories.dto.ParticipationDto;
import fr.formation.categories.mappers.ParticipationMapper;
import fr.formation.categories.models.Participation;
import fr.formation.categories.repositories.ParticipationRepository;

@Service
public class ParticipationService extends GenericService<ParticipationMapper, ParticipationRepository, Participation, ParticipationDto> {
	
}

package fr.formation.categories.mappers;

import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import fr.formation.categories.dto.FormateurDto;
import fr.formation.categories.dto.GroupeDto;
import fr.formation.categories.models.Formateur;
import fr.formation.categories.models.Groupe;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface FormateurMapper extends fr.formation.categories.mappers.Mapper<Formateur, FormateurDto> {


	@Mappings({
//		@Mapping(target = "nbStagiaires", source = "getStagiaires().size()")
	})
	GroupeDto modelToDto(Groupe source);

	@AfterMapping
	default void computeNombreAndMoyenne(Formateur src, @MappingTarget FormateurDto dto) {
		dto.setModulesNb(src.getModules().size());
	}
}

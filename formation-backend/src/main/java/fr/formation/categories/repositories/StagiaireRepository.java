package fr.formation.categories.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.formation.categories.models.Stagiaire;

@Repository
public interface StagiaireRepository extends JpaRepository<Stagiaire, Integer> {

	@Query("from Stagiaire s where s.prenom like %:q% or s.nom like %:q% or s.email like %:q%")
	public Collection<Stagiaire> search (String q);
	
	@Query("from Stagiaire s where s.prenom like %:q% or s.nom like %:q% or s.email like %:q%")
	public List<Stagiaire> search (String q, Pageable page);
	
	
}

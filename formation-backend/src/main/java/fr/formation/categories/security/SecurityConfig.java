package fr.formation.categories.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//	@Autowired
//	private UserDetailsService uds;

	private final RequestMatcher adminUrl = new OrRequestMatcher(
			new AntPathRequestMatcher("/groupes", "GET"),
			new AntPathRequestMatcher("/groupes/*", "GET"),
			new AntPathRequestMatcher("/groupes/*", "POST"),
			new AntPathRequestMatcher("/groupes/*", "PUT"),
			new AntPathRequestMatcher("/groupes/*", "DELETE"),
			new AntPathRequestMatcher("/stagiaires", "GET"),
			new AntPathRequestMatcher("/stagiaires/*", "GET"),
			new AntPathRequestMatcher("/stagiaires/*", "POST"),
			new AntPathRequestMatcher("/stagiaires/*", "PUT"),
			new AntPathRequestMatcher("/stagiaires/*", "DELETE"));
//
//	private final RequestMatcher adminOrAuthorUrl = new OrRequestMatcher(
//			new AntPathRequestMatcher("/post/{id}", "PUT"),
//			new AntPathRequestMatcher("/post/{id}", "DELETE"), 
//			new AntPathRequestMatcher("/comment/{id}", "PUT"),
//			new AntPathRequestMatcher("/comment/{id}", "DELETE"));
//
//	private final RequestMatcher adminOrSelfUrl = new OrRequestMatcher(
//			new AntPathRequestMatcher("/user/{id}/*", "GET"),
//			new AntPathRequestMatcher("/user/{id}", "PUT"));
//
//	private final RequestMatcher userUrl = new OrRequestMatcher(
//			new AntPathRequestMatcher("/post", "POST"),
//			new AntPathRequestMatcher("/post/{id}/like", "POST"),
//			new AntPathRequestMatcher("/post/{id}/dislike", "POST"), 
//			new AntPathRequestMatcher("/comment", "POST"),
//			new AntPathRequestMatcher("/comment/{id}/like", "POST"),
//			new AntPathRequestMatcher("/comment/{id}/dislike", "POST"));

//	private final RequestMatcher authenticatedUrl = new OrRequestMatcher();

//	private final RequestMatcher publicUrl = new OrRequestMatcher(new AntPathRequestMatcher("/post", "GET"),
//			new AntPathRequestMatcher("/post/{id}", "GET"), new AntPathRequestMatcher("/post/{id}/comments", "GET"),
//			new AntPathRequestMatcher("/comment", "GET"), new AntPathRequestMatcher("/comment/{id}", "GET"),
//			new AntPathRequestMatcher("/category", "GET"), new AntPathRequestMatcher("/category/**", "GET"));

//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(uds);
//	}


	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			// deactivate session
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			// no login
			.formLogin().disable()
			// no logout
			.logout().disable()
			// no csrf
			.csrf().disable()
			// url access rules
			.authorizeRequests()
				.requestMatchers(adminUrl).hasRole("ADMIN")
//				.requestMatchers(userUrl).hasRole("USER")
//				.requestMatchers(new AntPathRequestMatcher("/post/{id}", "PUT")).access("hasRole('ADMIN') or @contentService.findById(#id).get().author == principal.getUser()")
//				.requestMatchers(new AntPathRequestMatcher("/post/{id}", "DELETE")).access("hasRole('ADMIN') or @contentService.findById(#id).get().author == principal.getUser()")
//				.requestMatchers(new AntPathRequestMatcher("/comment/{id}", "PUT")).access("hasRole('ADMIN') or @contentService.findById(#id).get().author == principal.getUser()")
//				.requestMatchers(new AntPathRequestMatcher("/comment/{id}", "DELETE")).access("hasRole('ADMIN') or @contentService.findById(#id).get().author == principal.getUser()")
////				.requestMatchers(adminOrAuthorUrl).access("hasRole('ADMIN') or @contentService.findById(#id).get().author == principal.getUser()")
//				.requestMatchers(adminOrSelfUrl).access("hasRole('ADMIN') or #id == principal.getUser().id")
//				.requestMatchers(publicUrl).permitAll()
				.antMatchers("*").permitAll().and()
			.httpBasic();
	}

}

package fr.formation.categories.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.formation.categories.models.Personne;
import fr.formation.categories.services.PersonneService;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private PersonneService ps;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Personne p = ps.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("email " + username + "not found"));
		return new MyUserDetails(p);
	}

}

package fr.formation.categories.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@SuperBuilder
@Entity
public class Groupe {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	protected int id;
	
	@NotBlank
	@Length(min = 3)
	protected String nom;

	@ManyToMany
	@Builder.Default
	protected Set<Stagiaire> stagiaires = new HashSet();
}

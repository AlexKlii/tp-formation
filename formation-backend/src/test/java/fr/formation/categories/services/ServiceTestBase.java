package fr.formation.categories.services;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.formation.categories.repositories.FormateurRepository;
import fr.formation.categories.repositories.GroupeRepository;
import fr.formation.categories.repositories.ModuleRepository;
import fr.formation.categories.repositories.ParticipationRepository;
import fr.formation.categories.repositories.PersonneRepository;
import fr.formation.categories.repositories.StagiaireRepository;

@SpringBootTest
@EnableAutoConfiguration(exclude= {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
public class ServiceTestBase {

	@MockBean
	protected FormateurRepository fr;

	@MockBean
	protected GroupeRepository gr;

	@MockBean
	protected ModuleRepository mr;

	@MockBean
	protected ParticipationRepository par;

	@MockBean
	protected PersonneRepository per;
	
	@MockBean
	protected StagiaireRepository sr;

}
